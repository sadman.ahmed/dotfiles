function nvm
    set path_list $HOME/.nvm/nvm.sh /usr/share/nvm/nvm.sh /usr/local/opt/nvm/nvm.sh

    for path in $path_list
        if test -e $path
            set nvm_sh_path $path
            break
        end
    end

    bass source $nvm_sh_path --no-use ';' nvm $argv
end
