function l --description 'list files with exa' --wraps exa
    command exa --color always --icons --group-directories-first $argv
end
