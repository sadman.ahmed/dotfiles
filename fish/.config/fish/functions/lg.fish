function lg --description 'list git files with exa' --wraps exa
    command exa --color always --icons --long --group --group-directories-first --git $argv
end
