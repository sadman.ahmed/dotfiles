function glgdmd --description 'git pretty log with devmoji'
    command git log --graph --pretty=format:'%C(bold yellow)%h%Creset%C(bold magenta)%d%Creset %C(brightblack)-%Creset %Cgreen%ad %C(brightblack)-%Creset %C(bold blue)%an%Creset %C(brightblack)-%Creset %s' --abbrev-commit --decorate --color --date=format-local:'%Y-%m-%d %I:%M:%S%p %z' | devmoji --log --color | less -RF
end
