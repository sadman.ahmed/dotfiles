function la --description 'long list files (with hidden) using exa' --wraps exa
	# Use --git argument if the directory is a part of a git repository
	if git rev-parse --is-inside-work-tree &>/dev/null
        command exa --color always --icons --long --all --group --group-directories-first --binary --links --inode --blocks --git $argv
    else
        command exa --color always --icons --long --all --group --group-directories-first --binary --links --inode --blocks $argv
    end
end
