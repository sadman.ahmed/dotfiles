local function toggle_tabline_callback(event)
    return function()
        vim.opt.showtabline = 0
        vim.api.nvim_create_autocmd(event, {
            group = "ToggleTabline",
            pattern = "<buffer>",
            command = "lua vim.opt.showtabline = 2",
        })
    end
end

local function toggle_statusline_callback(event)
    return function()
        vim.opt.laststatus = 0
        vim.api.nvim_create_autocmd(event, {
            group = "ToggleStatusline",
            pattern = "<buffer>",
            command = "lua vim.opt.laststatus = 3",
        })
    end
end

vim.api.nvim_create_augroup("ToggleTabline", { clear = true })
vim.api.nvim_create_autocmd("FileType", {
    group = "ToggleTabline",
    pattern = "alpha",
    callback = toggle_tabline_callback("BufUnload"),
})

vim.api.nvim_create_augroup("ToggleStatusline", { clear = true })
vim.api.nvim_create_autocmd("FileType", {
    group = "ToggleStatusline",
    pattern = "alpha",
    callback = toggle_statusline_callback("BufUnload"),
})

vim.api.nvim_create_augroup("CustomHighlight", { clear = true })
vim.api.nvim_create_autocmd("ColorScheme", {
    group = "CustomHighlight",
    pattern = "*",
    callback = require("theme.utils").update_hl,
})
