local theme_utils = require("theme.utils")
local g = vim.g
local api = vim.api

g.gruvbox_material_palette = "material"
g.gruvbox_material_background = "soft"
g.gruvbox_material_cursor = "orange"
g.gruvbox_material_enable_bold = 1
g.gruvbox_material_enable_italic = 1
g.gruvbox_material_disable_italic_comment = 0
g.gruvbox_material_diagnostic_text_highlight = 1
g.gruvbox_material_diagnostic_line_highlight = 1
g.gruvbox_material_diagnostic_virtual_text = "colored"
g.gruvbox_material_current_word = "grey background"
g.gruvbox_material_statusline_style = "default"
g.gruvbox_material_better_performance = 1

api.nvim_cmd({cmd="colorscheme", args={"gruvbox-material"}}, {})
theme_utils.update_hl()
