local M = {}

local binary = "lua-language-server"
local project_path = "/usr/share/lua-language-server"
local runtime_path = vim.split(package.path, ";")

function M.setup(t)
    setmetatable(t, {__index={on_attach={}}})

    local on_attach = t[1] or t.on_attach

    table.insert(runtime_path, "lua/?.lua")
    table.insert(runtime_path, "lua/?/init.lua")

    require("lspconfig").sumneko_lua.setup {
        cmd = {binary, "-E", project_path .. "/main.lua"};
        settings = {
            Lua = {
                runtime = {
                    -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
                    version = "LuaJIT",
                    -- Setup your lua path
                    path = runtime_path
                },
                diagnostics = {
                    -- Get the language server to recognize the `vim` global
                    globals = {"vim"}
                },
                workspace = {
                    -- Make the server aware of Neovim runtime files
                    library = vim.api.nvim_get_runtime_file("", true)
                },
                -- Do not send telemetry data containing a randomized but unique identifier
                telemetry = {
                    enable = false
                }
            }
        },
        on_attach=on_attach
}
end


return M
