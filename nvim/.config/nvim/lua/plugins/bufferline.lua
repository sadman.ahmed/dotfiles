local colors = require("theme").colors

require("bufferline").setup({
    options = {
        diagnostics = "nvim_lsp",
        diagnostics_indicator = function(count)
            return "(" .. count .. ")"
        end,
        offsets = {
            {
                filetype = "NvimTree",
                text = "Explorer",
                text_align = "left",
                highlight = "Directory",
            },
            {
                filetype = "Outline",
                text = "Outline",
                text_align = "left",
            },
            {
                filetype = "minimap",
                text = "Map",
                text_align = "left",
            },
        },
        separator_style = { "", "" },
        hover = {
            enabled = true,
            delay = 0,
            reveal = { "close" },
        },
    },
    highlights = {
        fill = {
            bg = colors.bg1,
        },
    },
})
