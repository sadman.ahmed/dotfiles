local alpha = require("alpha")
local dashboard = require("alpha.themes.dashboard")
local colors = require("theme").colors

local get_output = function (cmd)
    local handler = io.popen(cmd, "r")
    local output = handler:read("*a")
    handler:close()

    local lines = {}
    for w in output:gmatch("([^\n]+)") do
        table.insert(lines, w)
    end

    return lines
end

dashboard.section.header.val = get_output("cat ~/.config/nvim/ascii/phoenix.txt")
dashboard.section.header.opts.hl = "AlphaHeader"
dashboard.section.buttons.val = {
    dashboard.button("e"      , "  New file" , ":ene <BAR> startinsert <CR>"),
    dashboard.button("SPC f r", "ﭯ  Open recent files"),
    dashboard.button("SPC f f", "  Find files"),
    dashboard.button("SPC f s", "  Find text"),
    dashboard.button("SPC f b", "  Open file browser"),
    dashboard.button("SPC f p", "  Open recent projects"),
    dashboard.button("SPC S l", "  Open last session"),
    dashboard.button("SPC f m", "  Jump to bookmarks"),
    dashboard.button("SPC f h", "  Find help"),
    dashboard.button("q", "  Quit" , ":qa<CR>")
}
dashboard.section.footer.val = get_output("fortune")
dashboard.section.footer.opts.hl = "AlphaFooter"
dashboard.opts.layout = {
    { type = "padding", val = 4 },
    dashboard.section.header,
    { type = "padding", val = 4 },
    dashboard.section.buttons,
    { type = "padding", val = 4 },
    dashboard.section.footer,
}

alpha.setup(dashboard.opts)

vim.cmd("highlight AlphaHeader gui=bold guifg="..colors.orange)
vim.cmd("highlight AlphaFooter gui=bold guifg="..colors.aqua)
