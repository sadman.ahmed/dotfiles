local null_ls = require("null-ls")
local mason_null_ls = require("mason-null-ls")
local on_attach = require("plugins.lspconfig").on_attach

local function register_null_ls_source(lsp_feature_name, source_name)
    if null_ls.is_registered(source_name) then
        return
    end

    local source_path = string.format("null-ls.builtins.%s.%s", lsp_feature_name, source_name)

    if pcall(require, source_path) then
        null_ls.register(null_ls.builtins[lsp_feature_name][source_name])
    end
end

null_ls.setup({
    on_attach = on_attach,
})
mason_null_ls.setup({
    handlers = {
        function(source_name)
            for lsp_feature_name in pairs(null_ls.builtins) do
                if lsp_feature_name ~= "_test" then
                    register_null_ls_source(lsp_feature_name, source_name)
                end
            end
        end,
    },
})
