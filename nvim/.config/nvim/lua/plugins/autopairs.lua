local npairs = require("nvim-autopairs")
local Rule = require("nvim-autopairs.rule")
local ts_conds = require("nvim-autopairs.ts-conds")
local cmp_autopairs = require("nvim-autopairs.completion.cmp")
local cmp = require("cmp")

npairs.setup({
	enable_check_bracket_line = true,
	-- ts_config = {
	--     lua = {'string'},-- it will not add pair on that treesitter node
	--     javascript = {'template_string'},
	--     java = false,-- don't check treesitter on java
	-- }
	disable_filetype = { "TelescopePrompt" },
	check_ts = true,
})

-- If you want insert `(` after select function or method item
cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done({ map_char = { tex = "" } }))

-- press % => %% is only inside comment or string
npairs.add_rules({
	Rule("%", "%", "lua"):with_pair(ts_conds.is_ts_node({ "string", "comment" })),
	Rule("$", "$", "lua"):with_pair(ts_conds.is_not_ts_node({ "function" })),
})
