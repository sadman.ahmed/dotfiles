local fn = vim.fn
local M = {}

local unpack = table.unpack or unpack

M.generate_tbl_extend_function = function(behavior, base_table)
    return function(...)
        return vim.tbl_extend(behavior, base_table, ...)
    end
end

M.truncate_list = function(list, len, reverse)
    local start_index, end_index = 1, len
    reverse = reverse or false

    if #list <= len then
        return list
    end

    if reverse then
        start_index, end_index = #list - len + 1, #list
    end

    local truncated_list = { unpack(list, start_index, end_index) }

    return truncated_list
end

M.truncate_path = function(path, len, reverse, sep)
    reverse = reverse or false
    sep = sep or "/"
    local path_list = fn.split(path, sep)

    if #path_list <= len then
        return path
    end

    local truncated_path_list = M.truncate_list(path_list, len, reverse)
    local truncated_path = table.concat(truncated_path_list, sep)

    return truncated_path
end

return M
