-- Tree
vim.g.loaded_netrwPlugin = 1

-- Miscellaneous
vim.g.mapleader = " "
vim.g.python3_host_prog = "$NVIM_PYTHON3_HOST_EXECUTABLE_PATH"
vim.g.node_host_prog = "$NVIM_NODE_HOST_EXECUTABLE_PATH"
