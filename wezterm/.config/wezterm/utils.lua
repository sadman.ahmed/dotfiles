local M = {}

M.find_shell_path = function(shell_name)
    shell_name = shell_name or "bash"

    local result = "/usr/bin/bash"
    local handle = assert(io.popen("which " .. shell_name))

    local temp_result = handle:read("*a"):gsub("%s+", "")

    if temp_result ~= "" then
        result = temp_result
    end

    handle:close()

    return result
end

return M
