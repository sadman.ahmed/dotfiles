local colors_config = require("config.colors")
local fonts_config = require("config.fonts")
local keys_config = require("config.keys")
local utils = require("utils")

return {
    front_end = "OpenGL",
    default_prog = { utils.find_shell_path("fish"), "-l" },
    use_fancy_tab_bar = false,
    hide_tab_bar_if_only_one_tab = true,
    bold_brightens_ansi_colors = false,
    cursor_blink_ease_in = "Constant",
    check_for_updates = false,
    cursor_blink_ease_out = "Constant",
    cursor_blink_rate = 500,
    default_cursor_style = "BlinkingBar",
    disable_default_key_bindings = true,
    leader = keys_config.leader,
    show_tab_index_in_tab_bar = false,
    switch_to_last_active_tab_when_closing_tab = true,
    font = fonts_config,
    window_padding = {
        left = 0,
        right = 0,
        top = 0,
        bottom = 0,
    },
    colors = colors_config,
    keys = keys_config.keys,
}
